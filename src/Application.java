import java.util.Scanner;
import java.util.Arrays;
public class Application {
    boolean runApp=true;
    int [] ar;
    boolean check1;
    boolean check2;
    String error="Создайте  массив";
    void runTheApp(){
        while(runApp){
            showMenu();
            processMenu();
        }

    }
    void showMenu(){
        System.out.println("0- Выход");
        System.out.println("1- Введите длину массива");
        System.out.println("2- Введите элементы массива");
        System.out.println("3- Сортировка по возрастанию");
        System.out.println("4- Сортировка по убыванию");
        System.out.println("5- Вывести элемент массива");
    }
    void processMenu(){
        int b=readTheUserInput("Сделайте выбор");
        switch (b){
            case 0:
                runApp=false;
                break;
            case 1:
                createAnArrays();
                break;
            case 2:
                fillAnArrays();
                break;
            case 3:
                sortAnArrays("По возростанию");
                break;
            case 4:
                sortAnArrays("По Убыванию");
                break;
            case 5:
                printAnElement();
                break;
                default:
                    System.out.println("Введите чило от 0 до 5");
                    break;
        }

    }
    int readTheUserInput(String g){
        System.out.println(g);
        Scanner input=new Scanner(System.in);
        int a;
        if(input.hasNextInt()){
            a=input.nextInt();
            return a;

        }
        else {
            System.out.println("Неверно");
            return readTheUserInput("Введите целое число");
        }

    }
    void createAnArrays(){
        ar=new int[readTheUserInput("Введите длину массива")];
        check1=true;
    }
    void fillAnArrays(){
        if(check1){
        for (int x=0;x<ar.length;x++) {
            ar[x]=readTheUserInput("Введите число массива с индексом "+x);
        }
        check2=true;
        }
        else{
            System.out.println(error);
        }
    }
    void sortAnArrays(String in){
        if(check1&&check2){
            String s="Массив отсортирован ";
            if(in=="По возростанию"){
                Arrays.sort(ar);
                System.out.println(s+in);
            }
            else{
                for (int m=0;m<ar.length;m++){
                    for(int n=1;n<ar.length;n++){
                        if(ar[n]>ar[n-1]){
                            int t;
                            t=ar[n-1];
                            ar[n-1]=ar[n];
                            ar[n]=t;
                        }
                    }
                }
                System.out.println(s+in);
            }

        }
        else{
            System.out.println(error);
        }
    }
    void printAnElement(){

        if(check1&&check2){
            int in=readTheUserInput("Ведите индекс элемента");
            if(in>=0&&in<ar.length){
                System.out.println(ar[in]);
            }
            else {
                System.out.println("Неверно");
                printAnElement();
            }
        }
        else{
            System.out.println(error);
        }

    }

}
